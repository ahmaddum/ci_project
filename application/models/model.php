<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
	Deskripsi		: Modal berinteraksi dengan database dan dipanggil di controller atau view
	Programmer 		: Akhmad Bakhrun
	Email			: abakhrun@aegis.co.id
	Nama Perusahaan	: PT. Aegis Ultima Teknologi
*/
class Model extends CI_Model {
	private $table;	
	private $fields= array();
	private $data= array();
	private $clause = array();
	private $query;
	
	public function __construct() 
	{
		parent::__construct();			
	}
	
	/* Entri data baru ke database
		@table untuk nama table tunggal
		@data satu atau lebih field dalam array
	*/
	public function insert($table,$data) 
	{  		
		return $this->db->insert($table,$data);
	}
	
	/* Menghapus data baru dari database
		@table untuk nama table tunggal
		@clause (optional) id data yang akan dihapus
	*/
	public function delete($table,$clause='') 
	{			
		if($clause!='')	
		{	//hapus data berdasarkan id
			$this->query= $this->db->delete($table,$clause); 
		}
		else 
		{	//hapus seluruh data
			$this->query= $this->db->delete($table); //$this->db->empty_table($table));$this->db->truncate($table));
		}	
		return $this->query;
	}
	/* Mengupdate data
		@table untuk nama table tunggal
		@data satu atau lebih field yang akan diupdate dalam array
		@clause (optional) id data yang akan diupdate
	*/
	public function update($table,$data,$clause='') 
	{	
		if ($clause!='')
		{	//Update data berdasarkan id
			return $this->db->update($table,$data,$clause);
		}
		else
		{	//Update seluruh data di table
			return $this->db->update($table,$data);
		}
	}
	
	
	/* Mengembalikan data yang dipilih dalam tipe object */
	public function select($table,$clause='') 
	{			
		if($clause!='')
		{	//Pilih data berdasarkan id
			$this->query=$this->db->get_where($table,$clause);
		}
		else
		{  // pilih semua data
			$this->query=$this->db->get($table);
		}
		
		return $this->query->result();
	}
	
	/* Mengembalikan total data */
	public function total($table='',$sql='')
	{	
		if($sql!='')
		{
			$this->query=$this->db->query($sql);
			$total=$this->query->num_row();
		}
		if($table!='')
		{
			$total=$this->db->count_all_results($table);
		}
		return $total;
	}	
			
	/* Mengembalikan data dalam tipe object array */
	public function query($sql)
	{		
		return $this->db->query($sql)->result();
	}	
	
	/* Mengupdate login session */
	public function last_act($session_id)
	{	
		return $this->db->update('user_logs',array('last_act_time'=>date('YmdHis')),array('session_id'=>$session_id));
	}	
}

/* application/models/jm.php */