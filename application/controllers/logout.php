<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function logout(){
	$this->session->sess_destroy();
	redirect(base_url('login'));
}
?>