<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles extends CI_Controller {
	public function index(){
		$data['data'] = $this->model->select('user_groups');
		$data['menu'] = $this->model->select('menu');
		$this->load->view('home/roles/index', $data);
	}

	public function id($id){
		$user = $this->model->select('user_group_role',array('user_group_id'=>$id));

		if($user > 0){
			echo json_encode(array('status' => 'ok', 'data' => $user));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}

	public function edit(){
		$id = $this->input->post('id');
		$menu = $this->input->post('menu-id');
		//delete 
		$this->model->delete('user_group_role',array('user_group_id'=>$id));

		//loop input
		for($i=0;$i<count($menu);$i++){
			$this->model->insert('user_group_role',array('user_group_id' => $id, 'menu_id' => $menu[$i]));			
		}

		//if($upd > 0){
		echo json_encode(array('status' => 'ok'));	
		//} else {
		//	echo json_encode(array('status' => 'error'));			
		//}
	}
}
