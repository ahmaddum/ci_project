<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Blog extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		$this->load->helper(array('form','url'));
					
		// if (!$this->session->userdata('logged_in')) {
		// 	redirect(base_url().'login');
		// }	
	}

	public function index(){	
		$sql="SELECT *
			FROM tbl_order
			WHERE status = ''
		";
		
		$data['data']=$this->model->query($sql);	
		$this->load->view('backend/pages/tables/data',$data);
	}

		function tambah(){
		$this->load->view('v_input');
	}
	
	// public function data(){	
	// 	$sql="SELECT *
	// 		FROM tbl_order
	// 		WHERE status = 'simpan'
	// 	";
		
	// 	$data['data']=$this->model->query($sql);	
	// 	$this->load->view('backend/pages/tables/data',$data);
	// }
	
	// public function tambah(){	
	// 	$sql="SELECT * FROM tbl_kategori WHERE status = 'aktif'";
	// 	$data['kategoris']=$this->model->query($sql);		
	// 	$sql="SELECT * FROM tbl_lokasi WHERE status = 'aktif'";
	// 	$data['lokasis']=$this->model->query($sql);	
	// 	$sql="SELECT * FROM tbl_blog ";
	// 	$data['nama']=$this->model->select('tbl_blog');		
	// 	$this->load->view('home/blog/tambah',$data);
	// }
	
	// public function history(){	
	// 	$sql="SELECT *
	// 		FROM tbl_blog
	// 		WHERE status = 'simpan'
	// 	";
		
	// 	$data['data']=$this->model->query($sql);	
	// 	$this->load->view('home/blog/history',$data);
	// }

	public function editblog(){	
		$query = $this->db->query('UPDATE tbl_order set status_notifikasi="0"'); // proses Refresh Permintaan order masuk
		if ($query) {
			$sql="SELECT *
				FROM tbl_order
			";
			
			$data['data']=$this->model->query($sql);	
			$this->load->view('backend/pages/tables/data',$data);
		} 
		
	}
	
	// public function blogkeluar(){	
	// 	$sql="SELECT *
	// 		FROM tbl_blog
	// 		WHERE status = 'simpan'
	// 	";
		
	// 	$data['data']=$this->model->query($sql);	
	// 	$this->load->view('home/blog/blogkeluar',$data);
	// }

	// public function terimablog(){	
	// 	$sql="SELECT * FROM tbl_kategori WHERE status = 'aktif'";
	// 	$data['kategoris']=$this->model->query($sql);		
	// 	$sql="SELECT * FROM tbl_lokasi WHERE status = 'aktif'";
	// 	$data['lokasis']=$this->model->query($sql);	
	// 	$data['satuans']=$this->model->select('tbl_satuan');		
	// 	$this->load->view('home/blog/terimablog',$data);
	// }

	// public function tes(){	
	// 	$id_blog=$this->input->post('id_blog');
	// 	$query = $this->db->query('UPDATE tbl_order set status_notifikasi="0"'); // proses Refresh Permintaan order masuk
	// 	if ($query) {
	// 		$sql="SELECT *
	// 			FROM tbl_order	
	// 			WHERE id_blog='".$id_blog."'
	// 		";
	// 		// echo $sql;
	// 		$data['data']=$this->model->query($sql);	
	// 		$this->load->view('backend/pages/tables/data',$data);
	// 	} 
		
	// }


	// public function permintaanblog(){	
	// 	$sql="SELECT * FROM tbl_kategori WHERE status = 'aktif'";
	// 	$data['kategoris']=$this->model->query($sql);		
	// 	$sql="SELECT * FROM tbl_lokasi WHERE status = 'aktif'";
	// 	$data['lokasis']=$this->model->query($sql);

	// 	# Mendapatkan notifikasi
	// 	// $sql_notifikasi = "SELECT * FROM tbl_order";

	// 	# Variable $notifikasi untuk ditampilkan
	// 	//$data['notifikasi'] = $this->model->query($sql_notifikasi);

	// 	$this->load->view('home/blog/permintaanblog',$data);
	// }
	
	// public function ambilblog(){	
	// 	$sql="SELECT * FROM tbl_kategori WHERE status = 'aktif'";
	// 	$data['kategoris']=$this->model->query($sql);		
	// 	$sql="SELECT * FROM tbl_lokasi WHERE status = 'aktif'";
	// 	$data['lokasis']=$this->model->query($sql);	
	// 	$data['satuans']=$this->model->select('tbl_satuan');		
	// 	$this->load->view('home/blog/ambilblog',$data);
	// }
	
	// public function kambing(){	
	// 	$sql="SELECT * FROM tbl_kategori WHERE status = 'aktif'";
	// 	$data['kategoris']=$this->model->query($sql);		
	// 	$sql="SELECT * FROM tbl_lokasi WHERE status = 'aktif'";
	// 	$data['lokasis']=$this->model->query($sql);	
	// 	$data['satuans']=$this->model->select('tbl_satuan');		
	// 	$this->load->view('home/blog/kambing',$data);
	// }

	public function save_permintaan(){	
		// echo $this->upload_gambar('userfile');
		$data=array(
			'judul_blog' => strtoupper($this->input->post('judul_blog')),
			'isi_blog' => strtoupper($this->input->post('isi_blog')),
			'tgl_pembuatan' => date("Y-m-d H:i:s"),
			'lokasiid' => $this->input->post('lokasi'),
			'status' => 'simpan',
			'createon' => date("Y-m-d H:i:s"),
			'createby' =>  $this->session->userdata('username')
		);

		if($this->model->update('tbl_order',$data))
		{	
			echo "1";
		}else{
			echo "0";
		}
	}

	public function save(){
		$alur="masuk";		
		$data=array(
			'judul_blog' => strtoupper($this->input->post('judul_blog')),
			'isi_blog' => strtoupper($this->input->post('isi_blog')),
			'tgl_pembuatan' => date("Y-m-d H:i:s"),
			'lokasiid' => $this->input->post('lokasi'),
			'status' => 'simpan',
			'alur' => 'blog Masuk',
			'createon' => date("Y-m-d H:i:s"),
			'createby' =>  $this->session->userdata('username')
			
			
		);

		if($this->model->insert('tbl_order',$data))
		{	
			echo "1";
		}else{
			echo "0";
		}
	}

	// public function cetaklabel(){	
	// 	$sql="SELECT *
	// 		FROM tbl_blog
	// 		WHERE status = 'simpan'
	// 	";
		
	// 	$data['data']=$this->model->query($sql);	
	// 	$this->load->view('home/blog/list2edit',$data);
	// }


	// public function cetak($id){	
	// 	$sql="SELECT a.*
	// 		FROM tbl_blog a
	// 		WHERE a.id='".$id."'";			
	
	// 	$data['data']=$this->model->query($sql);			
	// 	$this->load->view('home/blog/cetaklabel',$data);
	// }

	// public function edit($id){	
	// 	$sql="SELECT a.*
	// 		FROM tbl_cabang a
	// 		WHERE a.kode='".$id."'";			
	
	// 	$data['data']=$this->model->query($sql);			
	// 	$data['cabangs']=$this->model->select('tbl_cabang');		
	// 	$this->load->view('home/cabang/edit',$data);
	// }

	// public function update(){	
	// 	$clause	=array('kode'=>$this->input->post('id'));
	// 	$data=array(
	// 		'nama'=>$this->input->post('nama'),
	// 		'jenis'=>$this->input->post('jenis'),
	// 		'kode_parent'=>$this->input->post('pembina'),
	// 		'modifyon' => date("Y-m-d H:i:s"),
	// 		'modifyby' => 'ADMIN'		
	// 	);
		
	// 	if($this->model->update('tbl_cabang',$data,$clause))
	// 	{	
	// 		echo "1";
	// 	}else{
	// 		echo "0";
	// 	}
	// }

	// public function delete(){	
	// 	$this->model->delete('tbl_blog', array('id' => $this->input->post('id')));

	// }
	
	// public function map(){	
	// 	$sql="SELECT * FROM perspectives";
	// 	$data['data']=$this->model->query($sql);	
	// 	$this->load->view('home/kpi/map',$data);
	// }

	// public function get_issue(){		
	// 	$kode=$this->input->post('kode');
	
	// 	if(!empty($kode)) {
	// 		$where="WHERE a.kode=".$kode;
	// 	} else{
	// 		$where="";
	// 	}		
	// 	$sql="SELECT 
	// 		a.kode, 
	// 		a.nama,
	// 		a.jenis,
	// 		a.aktif,
	// 		a.kode_parent		
	// 		FROM tbl_cabang a ".$where;	
		
	// 	echo json_encode($this->model->query($sql));
	// }
	
	// public function unggah()
	// {
	// 	$config['upload_path'] = './uploads/';
	// 	$config['allowed_types'] = 'xls|pdf';
	// 	// $config['max_size']	= '100000';
	// 	$config['encrypt_name'] = false;

	// 	$this->load->library('upload', $config);

	// 	if (!$this->upload->do_upload('files'))
	// 	{
	// 		$msg = '{"success":false, "message": "'. $this->upload->display_errors() .'" }';
	// 	}
	// 	else
	// 	{
	// 		$uploaded_file = $this->upload->data();
	// 		$file_name = $uploaded_file['file_name'];
	// 		$uploadfile = $uploaded_file['orig_name'];
	// 		$location_file = $uploaded_file['full_path'];
	// 		$file_size = $uploaded_file['file_size'];
	// 		$file_type = $uploaded_file['file_type'];
	// 		// $file_name2 = date("Y-m-d H:i:s").'.pdf';
	// 		$msg = '{"files":[{"name":"'.$file_name.'","files":"'.$uploadfile.'","location":"'.$location_file.'","size":'.$file_size.',"type":"'.$file_type.'"}]}';
	// 	}
	// 	echo $msg;	
	// }

	// public function upload(){
 //        $fileName = time().$_FILES['file']['name'];
         
 //        $config['upload_path'] = './uploads/'; //buat folder dengan nama uploads di root folder
 //        // $config['file_name'] = $fileName;
 //        $config['allowed_types'] = 'xls|xlsx|csv|pdf';
 //        $config['max_size'] = 10000;
	// 	$config['encrypt_name'] = false;
         
 //        $this->load->library('upload');
 //        $this->upload->initialize($config);
         
 //        if(! $this->upload->do_upload('file') )
 //        $this->upload->display_errors();
             
 //        $media = $this->upload->data('file');
 //        $inputFileName = './uploads/'.$media['file_name'];
         
 //            $this->load->view('home/blog/permintaanblog');
 //    }
	
}
