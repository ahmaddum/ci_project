<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Users extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		//$this->jm->last_act($this->session->userdata('SID'));	
		if (!$this->session->userdata('logged_in')) {
			redirect('../v_login');
		}		
	}
	public function index()
	{	
		$data['users'] = $this->model->select("users", "id <> 1");
		$this->load->view('backend/p_admin',$data);
	}	


	public function user_list(){	
		$aColumns = array('ID','USERNAME','NAME','GROUP');
		$sql="SELECT a.id,a.username,a.name,b.name AS group_name FROM users a JOIN user_groups b ON (a.user_group_id=b.id) WHERE 1=1";
		$sql_total = "SELECT count(ID) AS jml FROM users";
		$this->model->datatable($sql,$aColumns,$sql_total);	
	}


	public function add(){	
		$data['data']=$this->model->select('user_groups');
		//$data['organizations']=$this->model->select('organizations');
		$this->load->view('home/users/add',$data);		
	}

	public function save(){	
		$data=array(		
			'username'=>$this->input->post('username'),
			'name'=>$this->input->post('name'),
			'email'=>$this->input->post('email'),
			'password'=>md5($this->input->post('password')),
			'user_group_id'=>$this->input->post('user_group_id'),
			'organization_id'=>$this->input->post('organization_id')
		);
		if(!$this->check_username($this->input->post('username')) && !$this->check_email($this->input->post('email'))){
			if($this->model->insert('users',$data))			
				echo "1";
			else
				echo "0";
		}else{
			echo "2";
		}
	}

	public function edit($id){			
		$sql="SELECT a.*,b.id as group_id,b.name as group_name FROM users a, user_groups b WHERE a.user_group_id=b.id AND a.id=".$id;			
		$data['user']=$this->model->query($sql);
		$data['data']=$this->model->select('user_groups');
		$data['organizations']=$this->model->select('organizations');
		$this->load->view('home/users/edit',$data);
	}
	
	public function edit_profile(){	
		$id=$this->session->userdata('user_id');
		$sql="SELECT * FROM users WHERE id=".$id;			
		$data['user']=$this->model->query($sql);
		$this->load->view('home/users/edit_profile',$data);
	}

	public function edit_password(){
		$id=$this->session->userdata('user_id');
		$sql="SELECT * FROM users WHERE id=".$id;			
		$data['user']=$this->model->query($sql);
		$this->load->view('home/users/edit_password',$data);	
	}
	
	public function update()
	{	
		$data=array(		
			'username'=>$this->input->post('username'),		
			'name'=>$this->input->post('name'),		
			'user_group_id'=>$this->input->post('user_group_id'),
			'organization_id'=>$this->input->post('organization_id')
		);
		$clause=array(		
			'id'=>$this->input->post('id')
		);	
	
		if($this->model->update('users',$data,$clause))				
			echo "1";
		else
			echo "0";

	}

	public function update_profile(){	
		$data=array(		
			'name'=>$this->input->post('name'),
			'email'=>$this->input->post('email')
			//'password'=>md5($this->input->post('new_password'))			
		);
		$clause=array(		
			'id'=>$this->input->post('id')
		);	
		if($this->model->update('users',$data,$clause))				
			echo "1";
		else
			echo "0";
	}

	public function update_password(){	
		$data=array('password'=>md5($this->input->post('new_password')));
		$clause=array(		
			'id'=>$this->input->post('id')
		);	
		if($this->model->update('users',$data,$clause))				
			echo "1";
		else
			echo "0";
	}

	public function delete(){	
		$clause =array(
			'id'=>$this->input->post('id')
		);

		if($this->model->delete('users',$clause)){
			echo "1";
		}
		else{
			echo "0";
		}
	}	

	public function check_username($username){	
		$clause =array(
			'username'=>$this->input->post('username')
		);
		$jml=$this->model->select('users',$clause);
		if(count($jml)==1){
			return true;
		}
		else{
			return false;
		}
	}	

	public function check_email($email){	
		$clause =array(
			'email'=>$this->input->post('email')
		);
		$jml=$this->model->select('users',$clause);
		if(count($jml)==1){
			return true;
		}
		else{
			return false;
		}
	}	

	public function IsExist(){	
		$username=$this->session->userdata('username');
		$password=md5($this->input->post('password'));
		$clause =array(
			'username'=>$username,
			'password'=>$password
		);
		$jml=$this->model->select('users',$clause);		
		if(count($jml)==0){
			echo "0";
		}else{
			echo "1";
		}
		
	}
	
	public function show($id){			
		$sql="SELECT a.*,b.id as group_id,b.name as group_name FROM users a, user_groups b WHERE a.user_group_id=b.id AND a.id=".$id;				
		$data['user']=$this->model->query($sql);
		$this->load->view('home/users/show',$data);
	}

	public function reset(){			
		$id=$this->input->post('id');
		$name=$this->input->post('name');
		$username=$this->input->post('username');
		$password=$this->input->post('password');		
		$email=$this->input->post('email');	
		$data_email=array(
			'name'=>$name,
			'username'=>$username,
			'password'=>$password,
			'email'=>$email	
		);
		$data=array('password'=>md5($password));
		$clause=array('id'=>$id);
			
		// $this->send_email($data_email);
		//echo $this->model->update('users',$data,$clause) ? "1":"0";
	}

	public function reset_password(){
		$new_password = rand(123456,999999);

		$data = array('password'=>md5($new_password));

		$clause=array(		
			'id'=>$this->input->post('id')
		);	

		if($this->model->update('users', $data, $clause))				
			echo json_encode(array('status' => 1, 'new_password' => $new_password));			
		else
			echo json_encode(array('status' => 0));
	}
}