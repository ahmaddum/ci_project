<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Groups extends CI_Controller {
	public function __construct() {
		parent::__construct();	
		if (!$this->session->userdata('logged_in')) {
			redirect('../login');
		}			
	}
	public function index(){	
		$data['data']=$this->model->select("user_groups");
		$this->load->view('home/groups/index',$data);
	}	
	public function id($id){
		$user = $this->model->select('user_groups',array('id'=>$id));

		if($user > 0){
			echo json_encode(array('status' => 'ok', 'data' => $user[0]));
		} else {
			echo json_encode(array('status' => 'error'));
		}
	}
	public function group_list(){	
		$aColumns = array('ID','NAME','GROUP');
		$sql="SELECT * FROM user_groups WHERE 1=1";
		$sql_total = "SELECT count(ID) AS jml FROM user_groups";
		$this->model->datatable($sql,$aColumns,$sql_total);	
	}	
	public function add(){			
		$data['groups']=$this->model->select("user_groups");
		$this->load->view('home/groups/add',$data);		
	}
	public function save(){	
		$data=array(			
			'name'=>$this->input->post('name'),
			'keterangan'=>$this->input->post('description')					
		);
		//$menu_id=explode(';',$this->input->post('menu_id'));

		if(!$this->check_exist($this->input->post('name'))){
			if($this->model->insert('user_groups',$data)){
				/*$user_group_id=$this->db->insert_id();
				for($i=0;$i<(count($menu_id)-1);$i++){		
					$data_menu=array(			
						'user_group_id'=>$user_group_id,
						'menu_id'=>$menu_id[$i]					
					);				
					$this->model->insert('user_group_role',$data_menu);
				}*/
				echo "1";
			}else{ 
				echo "0";
			}				
		}else{
			echo "2";
		}
	}
	public function edit($id){	
		$clause=array('id'=>$id);			
		$data['groups']=$this->model->select("user_groups");
		$data['data']=$this->model->select('user_groups',$clause);
		$this->load->view('home/groups/edit',$data);
	}
	public function update(){	
		
		$clause=array('id'=>$this->input->post('id'));
		$clause_delete=array('user_group_id'=>$this->input->post('id'));
		
		$data=array(			
			'name'=>$this->input->post('name'),
			'keterangan'=>$this->input->post('description')					
		);
		$menu_id=explode(';',$this->input->post('menu_id'));

		if(!$this->check_exist($this->input->post('name'),$this->input->post('id'))){				
			if($this->model->update('user_groups',$data,$clause)){	
				/*$this->model->delete('user_group_role',$clause_delete);				
				for($i=0;$i<(count($menu_id)-1);$i++){		
					$data_menu=array(			
						'user_group_id'=>$this->input->post('id'),
						'menu_id'=>$menu_id[$i]					
					);				
					$this->model->insert('user_group_role',$data_menu);
				}*/
				echo "1";
			}else{ 
				echo "0";
			}				
		}else{
			echo "2";
		}
	}
	public function delete(){	
		$clause =array('id'=>$this->input->post('id'));
		echo $this->model->delete('user_groups',$clause) ? "1":"0";
	}	
	public function check_exist($name,$id=''){	
		if($id !='')
			$sql="SELECT * FROM user_groups WHERE name='$name' AND id <> $id";
		else
			$sql="SELECT * FROM user_groups WHERE name='$name'";
			
		$jml=$this->model->query($sql);
		return (count($jml)==1) ? true : false;
	}	

}
