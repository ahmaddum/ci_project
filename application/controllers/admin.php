<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends MY_Controller{

 // 	public function __construct() {
	// 	parent::__construct();	
	// 	if (!$this->session->userdata('logged_in')) {
	// 		redirect(base_url().'login');
	// 	}					
	// }
// 	function __construct(){
// 		parent::__construct();
	
// 		if($this->session->userdata('status') != "login"){
// 			redirect(base_url("login"));
// 		}
// 	}
 
// 	function index(){
// 		$this->load->view('backend/p_admin');
// 	}
// }

	public function __construct() {
    parent::__construct();
    //memanggil function dari MY_Controller
    $this->cekLogin();
    //validasi jika session dengan level karyawan mengakses halaman ini maka akan dialihkan ke halaman karyawan
    if ($this->session->userdata('level') == "user") {
      redirect('user');
    }
}

  public function index()
  {
    
    $this->load->view('backend/index');
  }
}
?>