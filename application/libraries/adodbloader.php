 <?php if(!defined('BASEPATH')) exit('No direct script access allowed');
    class Adodbloader{
		function Adodbloader(){
                      if ( ! class_exists('ADONewConnection') )
                      {
                        require_once(APPPATH.'libraries/adodb/adodb.inc'.EXT);
                        require_once(APPPATH.'libraries/adodb/adodb-error.inc'.EXT);
                      }
					    $obj =& get_instance();
						$this->_init_adodb_library($obj);
			 
			  }
	
		
		function _init_adodb_library(&$ci) {
			 $db_var = false;
			 $debug = false;
			 $show_errors = true;
			 $active_record = false;
			 $db = NULL;
		   
			 if (!isset($dsn)) {
				// fallback to using the CI database file 
				include(APPPATH.'config/database'.EXT); 
				$group = 'default'; 
				$dsn = $db[$group]['dbdriver'].'://'.$db[$group]['username'] 
								 .':'.$db[$group]['password'].'@'.$db[$group]['hostname']
								 .'/'.$db[$group]['database']; 
			  }
		   
			  // Show Message Adodb Library PHP
			  if ($show_errors) {
				 require_once(APPPATH.'libraries/adodb/adodb-errorhandler.inc'.EXT);
			  }
		}
    }
