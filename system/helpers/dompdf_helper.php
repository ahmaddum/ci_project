<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
function pdf_create($html,$filename='',$stream=TRUE,$orientation=2) 
{
    require_once("dompdf/dompdf_config.inc.php");    

    $dompdf = new DOMPDF();
    $dompdf->load_html($html);
	
	switch($orientation){
		case 1:$dompdf->set_paper('a4', 'landscape');break;
		case 2:$dompdf->set_paper('a4', 'portrait');break;
		case 3:$dompdf->set_paper('a3', 'landscape');break;
	}
    $dompdf->render();
    if ($stream) {
        $dompdf->stream($filename.".pdf");
    } else {
        return $dompdf->output();
    }
}
?>