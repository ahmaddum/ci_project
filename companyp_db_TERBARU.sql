-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 23, 2018 at 09:53 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `companyp_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_berita`
--

CREATE TABLE `tbl_berita` (
  `berita_id` int(11) NOT NULL,
  `berita_judul` varchar(150) DEFAULT NULL,
  `berita_isi` text,
  `berita_image` varchar(40) DEFAULT NULL,
  `berita_tanggal` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_berita`
--

INSERT INTO `tbl_berita` (`berita_id`, `berita_judul`, `berita_isi`, `berita_image`, `berita_tanggal`) VALUES
(11, 'News 1', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, vero, provident, eum eligendi blanditiis ex explicabo vitae nostrum facilis asperiores dolorem illo officiis ratione vel fugiat dicta laboriosam labore adipisci.</p>\r\n', '26bb11c548af55638b847ccb9aaa4087.jpg', '2018-05-23 18:38:20'),
(12, 'News 2', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, vero, provident, eum eligendi blanditiis ex explicabo vitae nostrum facilis asperiores dolorem illo officiis ratione vel fugiat dicta laboriosam labore adipisci.</p>\r\n', 'cc42e44cce72efbe19e7cdc5070dcaa7.jpg', '2018-05-23 18:38:38'),
(13, 'News 3', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, vero, provident, eum eligendi blanditiis ex explicabo vitae nostrum facilis asperiores dolorem illo officiis ratione vel fugiat dicta laboriosam labore adipisci.</p>\r\n', 'af6bbae56fc2fc64d9979fd758bac5bc.jpg', '2018-05-23 18:38:52'),
(14, 'News 4', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, vero, provident, eum eligendi blanditiis ex explicabo vitae nostrum facilis asperiores dolorem illo officiis ratione vel fugiat dicta laboriosam labore adipisci.</p>\r\n', '85a9b2810f11b7b552a4ddfadb7af26a.jpg', '2018-05-23 18:39:07'),
(15, 'News 5', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, vero, provident, eum eligendi blanditiis ex explicabo vitae nostrum facilis asperiores dolorem illo officiis ratione vel fugiat dicta laboriosam labore adipisci.</p>\r\n', 'fcf8c0e42b59d1ccdc33c6302c3f6e8a.jpg', '2018-05-23 18:39:22'),
(16, 'News 6', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam, vero, provident, eum eligendi blanditiis ex explicabo vitae nostrum facilis asperiores dolorem illo officiis ratione vel fugiat dicta laboriosam labore adipisci.</p>\r\n', '50959019ddc3183e70f156311db8b3a1.jpg', '2018-05-23 18:39:37');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_order`
--

CREATE TABLE `tbl_order` (
  `id_blog` int(100) NOT NULL,
  `judul_blog` varchar(100) NOT NULL,
  `isi_blog` varchar(50) NOT NULL,
  `tgl_pembuatan` date NOT NULL,
  `lokasiid` varchar(50) NOT NULL,
  `createby` varchar(20) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_order`
--

INSERT INTO `tbl_order` (`id_blog`, `judul_blog`, `isi_blog`, `tgl_pembuatan`, `lokasiid`, `createby`, `status`) VALUES
(1, '0', 'TESTT2', '2018-02-02', '0', 'admin', ''),
(2, '0', 'BARANG BARANG', '2018-02-02', '0', 'admin', ''),
(3, '0', '131', '2018-02-02', '0', 'admin', ''),
(4, '0', '1233333', '2018-02-02', '0', 'admin', ''),
(5, '0', 'QWEASDASD', '2018-02-01', '0', 'admin', ''),
(6, '0', 'ASDADAS', '2018-02-02', '0', 'admin', ''),
(7, '0', 'CONTOH', '2018-02-02', '0', 'admin', ''),
(8, '0', 'ASDA', '2018-02-02', '0', 'admin', ''),
(9, '0', 'NGETEST1', '2018-02-02', '0', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `email` varchar(150) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `level` enum('admin','user','','') NOT NULL,
  `active` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `email`, `username`, `password`, `level`, `active`) VALUES
(1, 'admin@admin.com', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '1'),
(2, 'user@user.com', 'user', '24c9e15e52afc47c225b757e7bee1f9d', 'user', '1'),
(3, 'gshinta40@gmail.com', 'namaku', '25d55ad283aa400af464c76d713c07ad', 'user', '1'),
(4, 'banz.cozy@gmail.com', 'namamu', 'fcea920f7412b5da7be0cf42b8c93759', 'user', '1'),
(5, 'gueganteng@gmail.com', 'namague', '1cd5472c0c603b4ec530c63fa0b0756f', 'user', '1'),
(6, 'ngetest@gmail.com', 'ngetest', '208a457b210fc059a2a88145167b7eef', 'user', '1'),
(7, 'kepo@gmail.com', 'regis', '208a457b210fc059a2a88145167b7eef', 'user', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_berita`
--
ALTER TABLE `tbl_berita`
  ADD PRIMARY KEY (`berita_id`);

--
-- Indexes for table `tbl_order`
--
ALTER TABLE `tbl_order`
  ADD PRIMARY KEY (`id_blog`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_berita`
--
ALTER TABLE `tbl_berita`
  MODIFY `berita_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `tbl_order`
--
ALTER TABLE `tbl_order`
  MODIFY `id_blog` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
